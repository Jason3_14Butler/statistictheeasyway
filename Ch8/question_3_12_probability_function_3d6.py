# This Python script solves questions 3 and 12 from Statistics the Easy Way
# It calculates the probability function, cumulative distribution function,
# mean and variance for the total on 3 six sided dice

from array import *

# Histogram of the dice permutations that total a particular value
dice_total_hist = array('d', [0, 0,0 , 0, 0, 0, 0, 0, 0 ,0, 0, 0, 0,0 ,0 ,0, 0, 0, 0 ])
dice_prob = array('d', [0, 0,0 , 0, 0, 0, 0, 0, 0 ,0, 0, 0, 0,0 ,0 ,0, 0, 0, 0 ])
dice_cumlative_dist= array('d', [0, 0,0 , 0, 0, 0, 0, 0, 0 ,0, 0, 0, 0,0 ,0 ,0, 0, 0, 0 ])

for x in range (1, 7):
    for y in range (1, 7):
        for z in range (1, 7):
            dice_total_hist[x+y+z] += 1
            #if x+y+z == 7:
            #    print x,y,z

#print dice_total_hist
total = 0
for num in range (0,19):
    dice_prob[num] = dice_total_hist[num] / 216
    print num, dice_prob[num]
    total += dice_prob[num]

print "total: ", total

# Calculate Mean

total = 0
squared_expt = 0
for num in range (0,19):
    total+= num * dice_prob[num]
    squared_expt+= num**2 * dice_prob[num]


mean = total
var = squared_expt - mean **2

print "\n"
print "mean: ", mean, " var: ", var
print "\n"

print "Cumulative Distribution Function"
# Calculate the Cumulative Distribution
for num in range (0,19):
    for num2 in range (0,num+1):
        dice_cumlative_dist[num] += dice_prob[num2]
    print num, dice_cumlative_dist[num]
