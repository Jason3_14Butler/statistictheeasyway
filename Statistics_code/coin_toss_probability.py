from __future__ import division

# Statistics the Easyway
# Ch. 2 # 7
# read in the number of coin tosses and print the 
# probability for the number of heads from 
# h = 0 to h = n

import sys
import math

def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)

if(len(sys.argv) != 2):
    print "Must pass in number of heads on command line"       
    sys.exit(0)  
    #raise Exception('Must pass in number of heads on command line')

num_coins = int(sys.argv[1])

print("number of coin tosses: ", num_coins)
for x in range(0, num_coins+1):
    prob = int(nCr(num_coins, x)) / 2**num_coins
    print "p(",str(x),") = ", prob 


