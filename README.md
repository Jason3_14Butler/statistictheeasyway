# StatisticTheEasyWay

This repository contains code for the programming questions from
Statistics the Easy Way, Third edition by
Douglas Downing, Ph. D. and
Jeffrey Clark, Ph. D.


Ch8:

Question 3. The Probability Function for the roll of three six sided dice
Question 12. The Variance for the roll of three six sided dice
