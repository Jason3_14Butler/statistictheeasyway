#Statics the Easyway 
# Cp. 2 #5 
# Print all the results of tossing 6 coins

import itertools as it 
import numpy as np

coins = {'H','T'}
coins_list = list(coins)
tosses = [x for x in it.combinations_with_replacement(coins_list, 6)]

#print(tosses)

for x in tosses:
    perms = list(set(it.permutations(x)))
    for p in perms:
        print(p)
#    perms = np.unique(it.permutations(x)) 
#    for p in perms:
#        for y in p: 
#            print(y)
