#Statics the Easyway 
# Ch. 2 #38 
# Print all the results of tossing rolling 3 dice 

import itertools as it 
import math

def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)

roll = {1,2,3,4,5,6}
roll_list = list(roll)
rolls = [x for x in it.combinations_with_replacement(roll_list, 3)]

s3_c_6s = nCr(6+3-1,3)
s6_c_3s = nCr(3+6-1,6)

print("3_c_6: ", s3_c_6s)
print("6_c_3: ", s6_c_3s)

#print(rolls)

print(len(rolls))
for x in rolls:
    print(x)
